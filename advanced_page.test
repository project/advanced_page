<?php
/**
 * @file
 * Tests for advanced_page.module.
 */

/**
 * Test cases for Advanced Page
 */
class AdvancedPageTest extends DrupalWebTestCase {

  /**
   * Annouces the name and description of the tests.
   */
  public static function getInfo() {
    return array(
      'name' => 'Advanced Page functionality',
      'description' => 'Testing core functionality for advanced page',
      'group' => 'Advanced Page',
    );
  }

  /**
   * Enable modules and create users with specific permissions.
   */
  public function setUp() {
    parent::setUp('advanced_page');

    $super_permissions = array(
      'administer advanced page',
      'view advanced page list',
      'create advanced_page content',
      'edit own advanced_page content',
      'delete own advanced_page content',
      'edit any advanced_page content',
      'delete any advanced_page content',
      'access administration pages',
    );
    $this->super_user = $this->drupalCreateUser($super_permissions);

    $editor_permissions = array(
      'administer advanced page',
      'view advanced page list',
      'create advanced_page content',
    );
    $this->editor_user = $this->drupalCreateUser($editor_permissions);

    $owner_permissions = array(
      'create advanced_page content',
      'edit own advanced_page content',
      'delete own advanced_page content',
    );
    $this->own_user = $this->drupalCreateUser($owner_permissions);

    $this->authenticate_user = $this->drupalCreateUser();

  }

  /**
   * Test if the content type is created properly on install.
   */
  public function testBundle() {
    $bundles = field_info_bundles('node');
    if (array_key_exists('advanced_page', $bundles)) {
      $this->assertTrue('Advanced page content type exists.');
    }
  }

  /**
   * Test the users for the access permissions.
   */
  public function testAccess() {
    // 1. settings page.
    $this->checkPermAdvancedPageSettingsPage();
    // 2. listing page.
    $this->checkPermAdvancedPageListPage();
    // 3. create node.
    $this->checkPermAdvancedPageCreate();
    // 4. edit node.
    $this->checkPermAdvancedPageEdit();
    // 5. View node.
  }

  /**
   * Test access for settings page.
   */
  public function checkPermAdvancedPageSettingsPage() {
    // 1. User with permissions $this->super_user.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('admin/config/system/advanced-page/settings');
    $this->assertResponse(200);
    $this->assertTrue(t('User with "administer advanced page" can access advanced page settings.'));
    $this->drupalLogout();

    // 2. User without permissions  $this->authenticate_user.
    $this->drupalLogin($this->authenticate_user);
    $this->drupalGet('admin/config/system/advanced-page/settings');
    $this->assertResponse(403);
    $this->assertTrue(t('User without "administer advanced page" cannot access advanced page settings.'));
    $this->drupalLogout();

    // 3. Anonymous User UID (0).
    $this->drupalGet('admin/config/system/advanced-page/settings');
    $this->assertResponse(403);
    $this->assertTrue(t('Anonymous user cannot access advanced page settings.'));
  }

  /**
   * Test List page access.
   */
  public function checkPermAdvancedPageListPage() {
    // 1. User with permissions $this->super_user.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('admin/config/system/advanced-page');
    $this->assertResponse(200);
    $this->assertTrue(t('User with "view advanced page list" permission can access advanced page list.'));
    $this->drupalLogout();

    // 2. User without permissions $this->authenticate_user.
    $this->drupalLogin($this->authenticate_user);
    $this->drupalGet('admin/config/system/advanced-page');
    $this->assertResponse(403);
    $this->assertTrue(t('User without "view advanced page list" permission cannot access advanced page list.'));
    $this->drupalLogout();

    // 3. Anonymous User UID (0).
    $this->drupalGet('admin/config/system/advanced-page');
    $this->assertResponse(403);
    $this->assertTrue(t('Anonymous user cannot access advanced page list.'));
  }

  /**
   * Test for node creation.
   */
  public function checkPermAdvancedPageCreate() {
    // 1. User with permissions $this->super_user.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('node/add/advanced-page');
    $this->assertResponse(200);
    $this->assertTrue(t('User with "create advanced_page content" permission can create advanced page content.'));
    $this->drupalLogout();

    // 2. User without permissions $this->authenticate_user.
    $this->drupalLogin($this->authenticate_user);
    $this->drupalGet('node/add/advanced-page');
    $this->assertResponse(403);
    $this->assertTrue(t('User without "create advanced_page content" permission cannot create advanced page content.'));
    $this->drupalLogout();

    // 3. Anonymous User UID (0).
    $this->drupalGet('node/add/advanced-page');
    $this->assertResponse(403);
    $this->assertTrue(t('Anonymous user can not create advanced page content.'));
  }

  /**
   * Edit the node.
   */
  public function checkPermAdvancedPageEdit() {
    $node_details = array(
      'type' => 'advanced_page',
      'title' => 'testing advanced page',
      'field_advanced_page_status' => array(
        'und' => array(
          0 => array(
            'value' => 0,
          ),
        ),
      ),
      'uid' => $this->super_user->uid,
    );

    $node = $this->drupalCreateNode($node_details);

    // 1. User with permissions $this->super_user.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('node/' . $node->nid . '/edit');
    $this->assertResponse(200, 'User with "edit any advanced_page content" can edit advanced pages.');
    $this->drupalLogout();

    // 2. User with permissions $this->super_user.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('node/' . $node->nid . '/edit');
    $this->assertResponse(200, 'User with "edit own advanced_page content" can edit advanced pages.');
    $this->drupalLogout();

    // 3. Owner user with  permissions $this->own_user.
    $this->drupalLogin($this->own_user);
    $this->drupalGet('node/' . $node->nid . '/edit');
    $this->assertResponse(403, 'User with "edit own advanced_page content" cannot edit advanced pages created by other.');
    $this->drupalLogout();

    // 4. User without  permissions $this->authenticate_user.
    $this->drupalLogin($this->authenticate_user);
    $this->drupalGet('node/' . $node->nid . '/edit');
    $this->assertResponse(403, 'User without "edit own advanced_page content" and "edit any advanced_page content" can not edit advanced pages.');
    $this->drupalLogout();

    // 3. Anonymous User UID (0).
    $this->drupalGet('node/' . $node->nid . '/edit');
    $this->assertResponse(403, 'Anonymous user can not edit advanced pages.');

  }


  /**
   * Set and Tests the settings enabled for the Advanced Page.
   */
  public function testAdvancedPageSettings() {

    $this->drupalLogin($this->super_user);

    // Save settings.
    $post = array();
    $post['advanced_page_url'] = 1;
    $post['advanced_page_url_alias'] = '';
    $post['advanced_page_javascript_field'] = 1;
    $post['advanced_page_css_field'] = 1;
    $post['advanced_page_file_upload_field'] = 1;
    $post['advanced_page_allowed_ext'] = 'gif png jpg jpeg txt';
    $post['advanced_page_menu'] = 1;
    $post['form_id'] = 'advanced_page_settings_form';

    $this->drupalPost('admin/config/system/advanced-page/settings', $post, t('Save configuration'));

    // Check for the settings done.
    $this->assertFieldChecked('edit-advanced-page-url');
    $this->assertFieldChecked('edit-advanced-page-javascript-field');
    $this->assertFieldChecked('edit-advanced-page-css-field');

    if ($this->assertFieldChecked('edit-advanced-page-file-upload-field')) {
      $this->assertFieldById('edit-advanced-page-allowed-ext', 'gif png jpg jpeg txt', "Given Values are matched");
    }

    $this->assertFieldChecked('edit-advanced-page-menu');

    $this->drupalGet('node/add/advanced-page');

    // Get the values.
    $advanced9[] = $advanced_page_url                = variable_get('advanced_page_url', 0);
    $advanced9[] = $advanced_page_url_alias          = variable_get('advanced_page_url_alias', '');
    $advanced9[] = $advanced_page_javascript_field   = variable_get('advanced_page_javascript_field', 0);
    $advanced9[] = $advanced_page_css_field          = variable_get('advanced_page_css_field', 0);
    $advanced9[] = $advanced_page_file_upload_field  = variable_get('advanced_page_file_upload_field', 0);
    $advanced9[] = $advanced_page_allowed_ext        = variable_get('advanced_page_allowed_ext', '');
    $advanced9[] = $advanced_page_menu               = variable_get('advanced_page_menu', 0);

    if ($advanced_page_url) {
      $this->assertFieldById('edit-advanced-page-path-alias', '', "URL path settings is available");
    }

    if ($advanced_page_file_upload_field) {
      $flag = $this->assertFieldById('edit-field-advanced-page-attach-file-und-0-upload', '', "File upload field is available");

      if ($flag) {
        $this->assertRaw(t("Allowed file types: !types", array("!types" => "<strong>$advanced_page_allowed_ext</strong>")));
      }
    }

    if ($advanced_page_javascript_field) {
      $this->assertFieldById('edit-field-advanced-page-attach-js-und-0-upload', '', "JS uoload field is available");
    }

    if ($advanced_page_css_field) {
      $this->assertFieldById('edit-field-advanced-page-attach-css-und-0-upload', '', "CSS uoload field is available");
    }

    if ($advanced_page_menu) {
      $this->assertFieldById('edit-menu-enabled', '', "Menu setting is available");
    }

    $this->drupalLogout();
  }

  /**
   * Check for Fields settings working fine.
   */
  public function testAdvancedPageForPublishUnpublish() {
    // Create a node.
    $node_details = array(
      'type' => 'advanced_page',
      'title' => 'testing advanced page',
      'field_advanced_page_status' => array(
        'und' => array(
          0 => array(
            'value' => 0,
          ),
        ),
      ),
      'uid' => $this->super_user->uid,
    );
    $node = $this->drupalCreateNode($node_details);

    $this->drupalGet('node/' . $node->nid);
    $this->assertResponse(403, 'Unprivileged user can not access the "Unpublished" content.');

    // Mark the content as published.
    $this->drupalLogin($this->super_user);
    $this->drupalGet('node/' . $node->nid . '/edit');

    $edit = array();
    $edit['field_advanced_page_status[und]'] = 1;
    $this->drupalPost(NULL, $edit, t('Save'));
    $this->drupalLogout();

    $this->drupalGet('node/' . $node->nid);
    $this->assertResponse(200, 'Any user can view the "Published" content.');
  }
}
